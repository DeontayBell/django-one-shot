from django.shortcuts import render
from .models import TodoList

# Create your views here.


def list_todo(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}
    return render(request, "todos/list.html", context)
